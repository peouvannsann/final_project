from typing import List, Dict

class Player:

    def __init__(self, name) -> None:

        self.name: str = name
        self.score: int = 0
        self.point: int = 0
        self.card: list[str] = []
        self.boom: bool = False
        
    def get_name(self) -> str:
        return self.name

    def set_name(self, name) -> None:
        self.name = name

    def get_score(self) -> int:
        return self.score

    def set_score(self, score) -> None:
        self.score += score

    def get_point(self) -> int:
        return self.point

    def set_point(self, point) -> None:
        self.point = point

    def get_card(self) -> list[str]:
        return self.card

    def set_card(self, card) -> None:
        self.card = card
    
    def get_boom(self) -> bool:
        return self.boom

    def set_boom(self, boom) -> None:
        self.boom = boom