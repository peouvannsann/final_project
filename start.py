from card import Card
from game import Game
from player import Player
from bot import Bot
from typing import List, Dict, TextIO
import time


limit_score: int
number: int
index: int
add_card: str

# Object 
cards:Card = Card()
bot:Bot = Bot()
game:Game = Game()

# list
list_of_players: list[Player] = [bot]
exist_player: list[str] = []

# File 
file: TextIO
file = open('file.csv', 'w')
file = open('file.csv', 'a')
file.write('Player;')


def game_over():
    print(
        """
         _____      ___       ___  ___   _______
        /  ___|    /   |     /   |/   | |   ____| 
        | |       /    |    / /|   /| | |  |__
        | |  _   /  /| |   / / |__/ | | |   __|
        | |_| | /  ___ |  / /       | | |  |____
        \_____//_/   |_| /_/        |_| |_______|
        
         _____    _     _   ______   ______
        /  _  \  | |   / / | _____| |  _   \\
        | | | |  | |  / /  | |__    | |_|  |
        | | | |  | | / /   |  __|   |  _   /
        | |_| |  | |/ /    | |____  | | \  \\
        \_____/  |___/     |______| |_|  \__\\
        
        """
     )


def Start():
    
    game_start: bool = True
    loop_start: bool = True
    game_round: int = 1
    
    while True:
        # Limit_score to play
        try:
            limit_score =  int(input('\nPLEASE ENTER A LIMIT SCORE TO END GAME (SUGGEST 100) :) \n==> '))
            break
        except :
            print("INVALID NUMBER. :3")


    while loop_start:
        # amound_of_players
        try:
            number_of_players: int =  int(input("PLEASE ENTER NUMBER OF PLAYERS :D \n==> "))

            if 3 <= number_of_players <= 5:
                loop_start = False

            else:
                print("PLAYER ALLOWED BETWEEN 3 TO 5.\n")

        except:
            print("INVALID NUMBER. :3\n")


    for number in range(number_of_players):

        while True:

            player_info: str = input(f"\nPLAYER-{number + 1}: ").upper()

            if player_info == "":                
                print("INPUT EMPTY! PLEASE ENTER PLAYER NAME :( ")
                continue

            elif player_info in exist_player:
                print('PLAYER ALREADY EXISTED! :o ')
                continue

            else:
                list_of_players.append(Player(player_info))
                exist_player.append(player_info)
                break

    print('\n')


    # Write player_name to file.csv
    for index in range(1, len(list_of_players)):    
        file.write(f'{list_of_players[index].get_name()};')


    # Start_game
    while game_start:

        shuffle_card = cards.shuffle()   

        # To Decore
        player_cards: list[list[str]] = []
        player_scores: list[ int] = []        
        
        # Deal cards      
        for index in range(len(list_of_players)):
            list_of_players[index].set_card(game.split_card(shuffle_card, 2))

        # Game starting
        for index in range(len(list_of_players)):   

            # Clear when starting game
            game.clear()
            time.sleep(1.5)

            #  If BOOM
            if game.check_boom(cards.eligibility(), list_of_players[index].get_card()) == "Boom":

                print(f'=============== ROUND {game_round} ===============')
                time.sleep(1.5)

                list_of_players[index].set_boom(True)

                print(f'\n{list_of_players[index].get_name()} : BOOM , {list_of_players[index].get_card()}')

                input("\nPLEASE PRESS ENTER TO CONTINUE....")
                
                continue
            
            # Bot's turn decide to add a card
            if index == 0:
                if game.check_boom(cards.eligibility(), list_of_players[index].get_card()) < 7:
                    list_of_players[index].set_card(list_of_players[index].get_card() + game.split_card(shuffle_card, 1))
                
                list_of_players[index].set_point( int(game.check_boom(cards.eligibility(), list_of_players[index].get_card())))
                continue

            print(f'=============== ROUND {game_round} ===============')
            time.sleep(1.5)

            # print result
            print(f'\n{list_of_players[index].get_name()} : {list_of_players[index].get_card()} = {game.check_boom(cards.eligibility(),list_of_players[index].get_card())}')

            # Player add card        
            add_card = input('\nPRESS \'Y\' TO ADD CARD \n ==> ')


            if add_card == 'Y':
                list_of_players[index].set_card(list_of_players[index].get_card() + game.split_card(shuffle_card, 1))

                print(f'\n{list_of_players[index].get_name()} : {list_of_players[index].get_card()} = {game.check_boom(cards.eligibility(),list_of_players[index].get_card())}')
            else:
                print(f"\nEND OF PLAYER{index}'S TURN, GOOD LUCK :)")
                

            list_of_players[index].set_point( int(game.check_boom(cards.eligibility(), list_of_players[index].get_card())))

            # To store player's card in list
            player_cards.append(str(list_of_players[index].get_card()))
            
            input("\nPLEASE PRESS ENTER TO CONTINUOUS....")
            game.clear()

        # append score to file
        file.write('\n')
        file.write(f'Round {game_round};')


        for index in range(1, len(list_of_players)):

            bot_boom: bool = list_of_players[0].get_boom()
            player_boom: bool = list_of_players[index].get_boom()

            # Compare bot's boom with player's boom
            if bot_boom == True and player_boom == True:
                pass

            elif bot_boom == False and player_boom == True:
                list_of_players[index].set_score(20)

            elif bot_boom == True and player_boom == False:
                list_of_players[index].set_score(-20)

            
            # Compare bot's point with player's point
            else:
                # check greater
                check_greater: str = game.check_greater(list_of_players[0].get_point(), list_of_players[index].get_point())

                if check_greater == "Tie":
                    pass

                elif check_greater:
                    list_of_players[index].set_score(20)

                else:
                    list_of_players[index].set_score(-20)
                                          
            player_scores.append(str(list_of_players[index].get_score()))

            file.write(f'{list_of_players[index].get_score()};')
            
        name_len: int = len(sorted(exist_player, key=len)[-1])
        card_len: int = len(sorted(player_cards, key=len)[-1])
        score_len: int = len(sorted(player_scores, key=len)[-1])
        total_len: int = name_len + card_len + score_len + 10

        # Decorated_score_board
        print("="*total_len)
        print(f"{' '*((total_len-10)//2)}SCORE BOARD{' '*((total_len-10)//2)}")
        print("="*total_len)
        print(f"| {bot.get_name()}{' '*(name_len-len(bot.get_name()))} | {' '*(score_len)} | {bot.get_card()}{' '*(card_len - len(str(bot.get_card())))} |")


        for index in range(1, len(list_of_players)):

            player_name: str = list_of_players[index].get_name()
            player_score: int = list_of_players[index].get_score()
            player_card: list[list[str]] = list_of_players[index].get_card()

            print(f"| {player_name}{' '*(name_len-len(player_name))} | {' '*(score_len-len(str(player_score)))}{player_score} | {player_card}{' '*(card_len - len(str(player_card)))} |")

        print("="*total_len)


        # start game again 
        for index in range(len(list_of_players)):

            list_of_players[index].set_card([])

            list_of_players[index].set_point(0)

            list_of_players[index].set_boom(False)


        game_round +=1
        game_start = game.check_score(list_of_players, limit_score)    
        input('\nPLEASE PRESS ENTER TO CONTINUOUS....')
        game.clear()
        

    for index in game.ranking(game.players_score(list_of_players)):
        print(index)

    game_over()