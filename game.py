import random
import sys
from os import system, name


class Game:

    def __init__(self) -> None:
        pass

    def split_card(self, deck: list, amount: int) -> list[str]:

        random_list: list[str] = []
        index: int

        for index in range(amount):

            random_card = random.choice(deck)

            random_list.append(random_card)

            deck.pop(deck.index(random_card))

        return random_list

    def check_boom(self, eligibility: dict, player_cards: list) -> str:

        point: int = 0
        card_number: int = 1
        number_cards: int = len(player_cards)
        index: int

        for index in range(number_cards):

            if player_cards[index][card_number] in eligibility.keys():

                point += eligibility[player_cards[index][card_number]]

        if number_cards == 3:

            return [point % 20, point % 10][point < 20]

        elif number_cards == 2 and 10 <= point <= 20:

            point = point % 10

        return "Boom" if point == 8 or point == 9 else [point % 20, point % 10][point < 20]

    # To find a winner between 2 players
    def check_greater(self, player1: int, player2: int) -> str:

        if player1 < player2:

            return player2

        elif player1 == player2:

            return("Tie")

    def clear(self) -> None:

        if name == 'nt':

            _ = system('cls')

    def check_score(self, scores_list: list, limited_score: int) -> bool:

        scores: list[int] = []
        index: int

        for index in range(1, len(scores_list)):

            scores.append(scores_list[index].get_score())

        return max(scores) < limited_score

    def players_score(self, list_of_players: list) -> dict:

        players_score: dict = {}
        index: int

        for index in range(1, len(list_of_players)):

            players_score[list_of_players[index].get_name()] = list_of_players[index].get_score()

        return players_score

    def ranking(self, players_score: dict) -> list:

        ranking: list = []
        name: list
        scores: list
        index: int
        count: int = 0
        rank: int = 1

        scores = sorted(list(set(players_score.values())), reverse=True)
        names = list(players_score.keys())

        for index in range(len(scores)):

            if count > 1: 
                rank+=count-1

            count=0
            
            for name in names:

                if scores[index] == players_score[name]:
                    ranking += [f"Rank{rank}: {name}, {scores[index]} point's'."]
                    count += 1
            
            rank += 1

        return ranking
