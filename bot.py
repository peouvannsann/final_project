import game
from game import Game
from card import Card


game = Game()
cards = Card()

class Bot:

    def __init__(self) -> None:
        self.name: str = "Bot"
        self.point: int = 0
        self.card: list[str] = []
        self.boom: bool = False

    def decide_add_card(self) -> None:
        
        if game.check_boom(cards.eligibility(), self.card) < 7:

            game.split_card(self.card, cards.shuffle(), 1)
        
        self.point = int(game.check_boom(cards.eligibility(), self.card))

    def get_name(self) -> str:
        return self.name

    def set_name(self, name) -> None:
        self.name = name

    def get_point(self) -> int:
        return self.point

    def set_point(self, point) -> None:
        self.point = point

    def get_card(self) -> list[str]:
        return self.card

    def set_card(self, card) -> None:
        self.card = card
    
    def get_boom(self) -> bool:
        return self.boom

    def set_boom(self, boom) -> None:
        self.boom = boom