import random


class Card:

    def __init__(self)-> None:        
        
        self._formats: list[str] = "♥ ♦ ♣ ♠".split()
        
        self._numbers: list[str] = "A 2 3 4 5 6 7 8 9 T J Q K".split()
    

    def shuffle(self) -> list:

        result:list[str] = []

        for _format in range(len(self._formats)):
            
            for number in self._numbers:
                
                result.append(str(self._formats[_format]) + str(number))
        
        random.shuffle(result)

        return result

    def eligibility(self) -> dict:

        boom:dict = {}
        index:int

        for index in range(1, 10):

            if index == 1:
                boom["A"] = index
                continue
                
            boom[str(index)] = index
        
        return boom
  
